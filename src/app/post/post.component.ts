import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Post} from './post';

@Component({
  selector: 'hadardasim-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
  post:Post;
  tempPost:Post = {title:null,body:null,author:null};  // for ex 5
  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() cancelEvent = new EventEmitter<Post>();  // for ex 5
  isEdit:Boolean = false;
  editButtonText = "Edit";

  constructor() { }

  sendDelete(){
    this.deleteEvent.emit(this.post);
  }
  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = "Save" : this.editButtonText = "edit"

             if(this.isEdit){   /// for ex 5     
       this.tempPost.title = this.post.title;
       this.tempPost.body = this.post.body;
      // this.tempPost.author = this.post.author;
     } else {     
       this.cancelEvent.emit(this.post);   // need explanation ####
     }
  }
    onCancel(){ // for ex 5
    this.isEdit = false;
    this.post.title = this.tempPost.title;
    this.post.body = this.tempPost.body;
  //  this.post.author = this.tempPost.author;
    this.editButtonText = 'Edit';
  }

  ngOnInit() {
  }

}
