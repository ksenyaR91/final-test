import { Component, OnInit } from '@angular/core';  // this component for ex 6 class

@Component({
  selector: 'hadardasim-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
