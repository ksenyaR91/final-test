import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';  // ex 7 class
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {
 // private _url = 'http://jsonplaceholder.typicode.com/Invoices';    ex 7 class

invoicesObservable;    /// ex 7 class
  getInvoices(){
   /// return this._http.get(this._url).map(res => res.json()).delay(2000); ex 7 class delete this // ex 7 class get the users from firebase
/*                   delete this when we use "join" and instead writes whats below
   this.usersObservable = this.af.database.list('/users');                     
   return this.usersObservable; 
*/      
        this.invoicesObservable = this.af.database.list('/invoices').map(      /// use this for the join
      invoices =>{
        invoices.map(
          invoice => {
            invoice.posTitles = [];
            for(var p in invoice.invoices){
                invoice.posTitles.push(
                this.af.database.object('/invoices/' + p)
              )
            }
          }
        );
        return invoices;
      }
    )
     return this.invoicesObservable;
 	}

  
 // updateUser(user){
  //  let user1 = {name:user.name,email:user.email}  // from the ex with the cancel -> conected to users.component.ts
  //}

  addInvoice(invoice){       // ex 7 class
    this.invoicesObservable.push(invoice);
  }
  updateInvoice(invoice){     ///// ex 7 class      after that changes in users.component.ts
    let invoiceKey = invoice.$key;
    let invoiceData = {name:invoice.name, amount:invoice.amount};
    this.af.database.object('/invoices/' + invoiceKey).update(invoiceData);
  }
  deleteInvoice(invoice){                      //// ex 7 class     from users.component.ts
    let invoiceKey = invoice.$key;
    this.af.database.object('/invoices/' + invoiceKey).remove();
  }
  constructor(private af:AngularFire) { }   ////   private _http:Http  is was like that in ex 6 and changed in ex 7 class

}

