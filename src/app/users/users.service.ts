import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';  // ex 7 class
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class UsersService {
 // private _url = 'http://jsonplaceholder.typicode.com/users';    ex 7 class

usersObservable;    /// ex 7 class
  getUsers(){
   /// return this._http.get(this._url).map(res => res.json()).delay(2000); ex 7 class delete this // ex 7 class get the users from firebase
/*                   delete this when we use "join" and instead writes whats below
   this.usersObservable = this.af.database.list('/users');                     
   return this.usersObservable; 
*/      
        this.usersObservable = this.af.database.list('/users').map(      /// use this for the join
      users =>{
        users.map(
          user => {
            user.posTitles = [];
            for(var p in user.posts){
                user.posTitles.push(
                this.af.database.object('/posts/' + p)
              )
            }
          }
        );
        return users;
      }
    )
     return this.usersObservable;
 	}

  
 // updateUser(user){
  //  let user1 = {name:user.name,email:user.email}  // from the ex with the cancel -> conected to users.component.ts
  //}

  addUser(user){       // ex 7 class
    this.usersObservable.push(user);
  }
  updateUser(user){     ///// ex 7 class      after that changes in users.component.ts
    let userKey = user.$key;
    let userData = {name:user.name, email:user.email};
    this.af.database.object('/users/' + userKey).update(userData);
  }
  deleteUser(user){                      //// ex 7 class     from users.component.ts
    let userKey = user.$key;
    this.af.database.object('/users/' + userKey).remove();
  }
  constructor(private af:AngularFire) { }   ////   private _http:Http  is was like that in ex 6 and changed in ex 7 class

}
